" ================ vundle ================
set nocompatible

" required for vundle
filetype off

" Setting up Vundle - the vim plugin bundler
let iCanHazVundle=1
let vundle_readme=expand('~/.vim/bundle/vundle/README.md')
if !filereadable(vundle_readme)
    echo "Installing Vundle..."
    echo ""
    silent !mkdir -p ~/.vim/bundle
    silent !git clone https://github.com/gmarik/vundle ~/.vim/bundle/vundle
    let iCanHazVundle=0
endif
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" ================ Bundles ================
Bundle 'Syntastic'
Bundle 'altercation/vim-colors-solarized'


" ================ General Config ====================

"set number                      "Line numbers are good
set backspace=indent,eol,start  "Allow backspace in insert mode
set history=1000                "Store lots of :cmdline history
set showcmd                     "Show incomplete cmds down the bottom
set showmode                    "Show current mode down the bottom
set gcr=a:blinkon0              "Disable cursor blink
set visualbell                  "No sounds
set autoread                    "Reload files changed outside vim
set modeline                    "Enable modelines in .py files

" This makes vim act like all other editors, buffers can
" exist in the background without being in a window.
" http://items.sjbach.com/319/configuring-vim-right
set hidden

"turn on syntax highlighting
syntax on

filetype indent plugin on

set virtualedit=onemore         " Allow for cursor beyond last character

" ============== Mouse ================
set mouse=a                 " Automatically enable mouse usage
set mousehide               " Hide the mouse cursor while typing


" ============== Tabs and indentation ================
set tabstop=8                   " Hard tabs are 8 characters
set expandtab                   " .. but I use soft tabs
set softtabstop=4               " .. at 4 spaces
set shiftwidth=4
set autoindent
"set textwidth=79               " Causes an automatic line wrap

filetype plugin on
filetype indent on

" Display tabs and trailing spaces visually
set listchars=tab:\ \ ,trail:·
set list

set nowrap       "Don't wrap lines
set linebreak    "Wrap lines at convenient points


" ================ Search Settings  =================

set incsearch        "Find the next match as we type the search
set hlsearch         "Hilight searches by default
set viminfo='100,f1  "Save up to 100 marks, enable capital marks


" ================ Clipboard ================
set clipboard=unnamed           "Use the system clipboard

if has("macunix")
    " Yank text to the OS X clipboard
    noremap <leader>y "*y
    noremap <leader>yy "*Y

    " Preserve indentation while pasting text from the OS X clipboard
    noremap <leader>p :set paste<CR>:put  *<CR>:set nopaste<CR>
endif 


" ================ Keybindings ================

" Fix home and end keybindings for screen, particularly on mac
" - for some reason this fixes the arrow keys too. huh.
map [F $
imap [F $
map [H g0

" ================ Turn Off Swap Files ==============

set noswapfile
set nobackup
set nowb

" ================ Persistent Undo ==================

" Keep undo history across sessions, by storing in file.
" Only works all the time.
silent !mkdir ~/.vim/backups > /dev/null 2>&1
set undodir=~/.vim/backups
set undofile


" ================ Folds ============================

set foldmethod=indent   "fold based on indent
set foldnestmax=3       "deepest fold is 3 levels
set nofoldenable        "dont fold by default


" ================ Scrolling ========================

set scrolloff=8         "Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=1


" ============== Python settings ====================
"python remove trailing whitespace
autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``
"python highlighting extras
let python_highlight_all = 1

source ~/.vimrc-python          " PEP 8 recommended settings

let g:syntastic_python_checkers=['pylint']

" ================== Styling ========================
set background=dark
colorscheme koehler
"colorscheme default
"colorscheme solarized
let g:solarized_termcolors=256
let g:solarized_termtrans=0

set colorcolumn=81  " Highlight the 81th column
hi ColorColumn ctermbg=darkblue guibg=#666666

if has("gui_macvim")
    set guifont=Menlo:h12.00
    set transparency=0
endif


" Set extra options when running in GUI mode
if has("gui_running")
    set guioptions-=T
    set guioptions+=e
    set t_Co=256
    set guitablabel=%M\ %t

    set lines=60
    set columns=100
    set mousefocus
    if &background == "dark"
        hi normal guibg=black
        set transp=8
    endif
endif

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

let g:indent_guides_auto_colors = 0
let g:indent_guides_guide_size = 1
let g:indent_guides_start_level = 2
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=black   ctermbg=3
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=darkgray ctermbg=4


" =============== Other Imports =====================
if filereadable(expand("~/.vimrc-local"))
  source ~/.vimrc-local
endif

" =============== Auto Install Bundles =================
if iCanHazVundle == 0
    echo "Installing Bundles, please ignore key map error messages"
    echo ""
    :BundleInstall
endif

